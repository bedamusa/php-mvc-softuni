<?php
namespace SoftUni\Config;

class DbConfig
{
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = 'S0ftUn1';
    const DB_NAME = 'blog';
}